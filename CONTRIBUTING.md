# Panduan Berkontribusi pada Proyek

Terima kasih telah berminat untuk berkontribusi pada proyek ini! Kami sangat menghargai setiap kontribusi yang Anda berikan untuk proyek ini. Berikut ini adalah panduan tentang cara berkontribusi pada proyek ini.

## Langkah-langkah untuk Berkontribusi

1. Fork repositori ini dan buat branch baru untuk setiap fitur atau perbaikan yang ingin Anda usulkan.

2. Lakukan perubahan pada branch tersebut.

3. Pastikan bahwa kode yang Anda tulis telah diuji dan bekerja dengan baik.

4. Buat pull request ke repositori utama dengan deskripsi yang jelas tentang apa yang telah Anda ubah.

## Standar Kode

Pastikan bahwa kode yang Anda tulis sesuai dengan standar kode yang telah ditetapkan di proyek ini. Setiap kontribusi harus mematuhi standar kode ini agar mudah dipahami dan diuji oleh pengembang lain.

## Pedoman Komunikasi

Kami mendorong para kontributor untuk berkomunikasi dengan sopan dan menghormati pendapat orang lain. Kritik yang membangun dan saran yang konstruktif sangat dihargai.

## Pelaporan Bug dan Permintaan Fitur

Jika Anda menemukan bug atau ingin mengusulkan fitur baru, buatlah tiket pada halaman issue proyek dan jelaskan masalah atau fitur yang Anda temukan dengan jelas dan detail.

## Lisensi

Dengan berkontribusi pada proyek ini, Anda setuju untuk memberikan lisensi yang sama dengan lisensi proyek.

Terima kasih atas kontribusi Anda!
