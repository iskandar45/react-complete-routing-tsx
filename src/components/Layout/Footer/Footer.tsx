import React from 'react'
import { Box, Container, Grid, Typography } from '@mui/material'
import { useTheme } from '@mui/material/styles'

const Footer = () => {
    const theme = useTheme()

    return (
        <Box sx={{ bgcolor: theme.palette.primary.main }} style={{ display: 'flex' }}>
            <Container>
                <Grid container spacing={2} color="white" display="flex">
                    <Grid item display="flex" flexDirection="column">
                        <Typography variant="h6">About Us</Typography>
                        <Typography variant="body1" align="justify">
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
                            ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                        </Typography>
                    </Grid>
                    <Grid item>
                        <Typography variant="h6">Product</Typography>
                        <div>
                            <ul>
                                <li>Arabic</li>
                                <li>English</li>
                                <li>Indonesia</li>
                                <li>Mandarin</li>
                            </ul>
                            <ul>
                                <li>Arabic</li>
                                <li>English</li>
                                <li>Indonesia</li>
                                <li>Mandarin</li>
                            </ul>
                        </div>
                    </Grid>
                    <Grid item>
                        <Typography variant="h6">Address</Typography>
                        <Typography variant="body1" align="justify">
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque.
                        </Typography>
                        <Typography variant="h6">Contact Us</Typography>
                    </Grid>
                </Grid>
            </Container>
        </Box>
    )
}

export default Footer
