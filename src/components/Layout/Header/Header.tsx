import React from 'react'
import styles from './styles.module.css'
import { AppBar, Toolbar, Typography, Button, Box, Container } from '@mui/material'
import logoImg from '../../../assets/img/logo.svg'

const Header = () => {
    return (
        <AppBar position="static" color="transparent" elevation={1} className={styles.navbar}>
            <Container>
                <Toolbar>
                    <Box sx={{ display: 'flex', alignItems: 'center', flexGrow: 1 }}>
                        <img src={logoImg} alt="Logo" style={{ height: 50, marginRight: 10 }} />
                        <Typography variant="h5" sx={{ display: { xs: 'none', sm: 'block' } }}>
                            Language
                        </Typography>
                    </Box>
                    <Box sx={{ display: 'flex', gap: '16px' }}>
                        <Button color="primary" variant="contained" className={styles.btnNavbar}>
                            Login
                        </Button>
                        <Button color="secondary" variant="contained" className={styles.btnNavbar}>
                            Register
                        </Button>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    )
}

export default Header
