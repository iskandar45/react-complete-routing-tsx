import React from 'react'
import Header from './Header/Header'
import Footer from './Footer/Footer'

type Props = {
    children: React.ReactNode
    hideFooter?: boolean
}

const Layout: React.FC<Props> = ({ children, hideFooter }) => {
    return (
        <>
            <Header />
            {children}
            {!hideFooter && <Footer />}
        </>
    )
}

export default Layout
