import React from 'react'
import styles from './styles.module.css'
import { Section1, Section2, Section3, Section4, Section5 } from './sections'

export const LandingPage: React.FC = () => {
  return (
    <div className="container">
      <div className={styles.content}>
        <Section1 />
        <Section2 />
        <Section3 />
        <Section4 />
        <Section5 />
      </div>
    </div>
  )
}
