import EmailConfirm from './EmailConfirm'
import SuccessPurchase from './SuccessPurchase'

export { EmailConfirm, SuccessPurchase }
