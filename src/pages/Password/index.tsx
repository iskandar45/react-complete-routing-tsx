import NewPassword from './NewPassword'
import ResetPassword from './ResetPassword'

export { NewPassword, ResetPassword }
