import React from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Layout from './components/Layout'
import { NotFoundPage } from './pages/NotFound'
import { LandingPage } from './pages/Landing'
import { LoginPage } from './pages/Login'
import { RegisterPage } from './pages/Register'
import { ROUTES } from './resources/routes-constants'
import './assets/styles/main.sass'
import { EmailConfirm, SuccessPurchase } from './pages/Success'
import { NewPassword, ResetPassword } from './pages/Password'
import { DetailCourse, MenuCourse } from './pages/Course'
import { Checkout } from './pages/Checkout'
import { Invoice, InvoiceDetail } from './pages/Invoice'

const App: React.FC = () => {
  const shouldHideFooter = [
    ROUTES.LOGIN,
    ROUTES.REGISTER,
    ROUTES.RESET_PASSWORD,
    ROUTES.NEW_PASSWORD,
    ROUTES.EMAIL_CONFIRM,
    ROUTES.CHECKOUT,
    ROUTES.PURCHASE_CONFIRM
  ].includes(location.pathname)

  return (
    <Router>
      <Layout hideFooter={shouldHideFooter}>
        <Routes>
          <Route path="*" element={<NotFoundPage />} />
          <Route path={ROUTES.HOMEPAGE_ROUTE} element={<LandingPage />} />
          <Route path={ROUTES.LOGIN} element={<LoginPage />} />
          <Route path={ROUTES.REGISTER} element={<RegisterPage />} />
          <Route path={ROUTES.EMAIL_CONFIRM} element={<EmailConfirm />} />
          <Route path={ROUTES.PURCHASE_CONFIRM} element={<SuccessPurchase />} />
          <Route path={ROUTES.RESET_PASSWORD} element={<ResetPassword />} />
          <Route path={ROUTES.NEW_PASSWORD} element={<NewPassword />} />
          <Route path={ROUTES.CHECKOUT} element={<Checkout />} />
          <Route path={ROUTES.INVOICE} element={<Invoice />} />
          <Route path={ROUTES.INVOICE_DETAIL} element={<InvoiceDetail />} />
          <Route path={ROUTES.MENU_COURSE} element={<MenuCourse />} />
          <Route path={ROUTES.DETAIL_COURSE} element={<DetailCourse />} />
        </Routes>
      </Layout>
    </Router>
  )
}

export default App
