import React from 'react'
import RootComponent from './RootComponent'
import { ThemeProvider } from '@emotion/react'
import theme from './utility/theme'

const App: React.FC = () => {
    return (
        <ThemeProvider theme={theme}>
            <RootComponent />
        </ThemeProvider>
    )
}

export default App
