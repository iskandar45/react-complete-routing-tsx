export const ROUTES = {
  HOMEPAGE_ROUTE: '/',
  LOGIN: '/login',
  REGISTER: '/register',
  EMAIL_CONFIRM: '/email-confirm',
  PURCHASE_CONFIRM: '/success-purchase',
  RESET_PASSWORD: '/reset-password',
  NEW_PASSWORD: '/new-password',
  CHECKOUT: '/checkout',
  MYCLASS: '/my-class',
  INVOICE: '/invoice',
  INVOICE_DETAIL: '/invoice/:id',
  MENU_COURSE: '/menu/:category',
  DETAIL_COURSE: '/menu/:category/:id'
}
