import { createTheme } from '@mui/material'
import React from 'react'

const theme = createTheme({
    palette: {
        primary: {
            main: '#226957'
        },
        secondary: {
            main: '#EA9E1F'
        }
    },
    typography: {
        fontFamily: [
            'Montserrat',
            'Roboto',
            'Segoe UI',
            'Roboto',
            'Oxygen',
            'Ubuntu',
            'Cantarell',
            'Fira Sans',
            'Droid Sans',
            'Helvetica Neue',
            'sans-serif'
        ].join(',')
    }
})

export default theme
